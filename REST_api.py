from flask import Flask, jsonify
from flask_restful import Resource, Api
from sqlalchemy import create_engine
import json 
import psycopg2

#########################
# ŁĄCZENIE SIE Z SERWEREM
DB_NAME = "test_db"
DB_USER = "postgres"
DB_PASS = "cytrus1"
DB_HOST = "localhost"
DB_PORT = "5432"

try:
    conn = psycopg2.connect(database = DB_NAME, user = DB_USER,
                            password = DB_PASS, host = DB_HOST, port = DB_PORT)

    print("Polaczono")
    cur = conn.cursor()

except:
    print("Blad")

db_connect = create_engine('postgresql://', creator=conn)
app = Flask(__name__)
api = Api(app)

class Authors(Resource):
    def get(self):         
        try:
            cur.execute("SELECT * FROM authors")   
            data = cur.fetchall()        
            result = {data[i][1]: data[i][2] for i in range(0, len(data))}              
            return json.dumps(result, indent=2)        
        except:
            return False
                   
class Stats(Resource):
    def get(self):
        try:
            cur.execute("SELECT * FROM stats")   
            data = cur.fetchall()        
            result = {data[i][1]: data[i][2] for i in range(0, len(data))}  
            return json.dumps(result, indent=2)   
        except:
            return False
                        
class Stats_Author(Resource):
    def get(self, author): 
        try:
            cur.execute("SELECT * FROM " + author)   
            data = cur.fetchall()        
            result = {data[i][1]: data[i][2] for i in range(0, len(data))}
            return json.dumps(result, indent=2) 
        except:
            return False           

api.add_resource(Authors, '/authors') 
api.add_resource(Stats, '/stats') 
api.add_resource(Stats_Author, '/stats/<author>') 

if __name__ == '__main__':
     app.run(port='8080')