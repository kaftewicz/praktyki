import psycopg2
import operator

#########################
# ŁĄCZENIE SIE Z SERWEREM
DB_NAME = "test_db"
DB_USER = "postgres"
DB_PASS = "cytrus1"
DB_HOST = "localhost"
DB_PORT = "5432"

try:
	conn = psycopg2.connect(database = DB_NAME, user = DB_USER,
							password = DB_PASS, host = DB_HOST, port = DB_PORT)


	print("Polaczono")
	cur = conn.cursor()

except:
	print("Blad")



#############################
# TWORZENIE TABELI Z AUTORAMI 
cur.execute("""CREATE TABLE authors	
				(
				ID SERIAL PRIMARY KEY NOT NULL,
				AUTOR TEXT NOT NULL,
				AUTOR_NAME TEXT NOT NULL
				)""")




###########################################
# OBLICZANIE LICZBY SŁÓW DLA KAŻDEGO AUTORA

r = ['\\n','i','a','oraz','lub','na','w','dla','do',
'z','nie','-','się','to','none','o','że','co','none)',"'"]		# znaki usuwane z tekstu


stats = dict() 	# tworzenie zbioru policzonych słów
s = []			# zbiór słów danego autora
s_all = []		# zbiór wszystkich słów

# pobieranie liczby wierszy/autorów
cur.execute("SELECT COUNT(AUTOR) FROM Dane *") 
row_number = cur.fetchone()[0]


for index in range(0, row_number):	

	cur.execute("SELECT * FROM Dane WHERE ID = %s",(index+1,))		# pobieranie tekstów każdego autora
	data = cur.fetchone()	

	s = str(data[2::]).lower().replace(',','').replace('.','').replace('!','').replace('?','').split()					# usuwanie znaków (. , ! ?) i zamiana na listę słów

	for symbol in r:
		while symbol in s:
			s.remove(symbol)	# usuwanie znaków
	
	s_all += s					# dodawanie słów do zbioru wszystkich słów

	for word in s:
		if word in stats:
			stats[word] += 1
		else:
			stats[word] = 1	


	# sortowanie słów
	sorted_stats = sorted(stats.items(), key=operator.itemgetter(1))
	results = sorted_stats[-10::]


	cur.execute("SELECT AUTOR FROM Dane WHERE ID = %s",(index + 1,))	# pobieranie autora artykułu 
	author_name = cur.fetchone()[0]
	author = author_name.lower().replace(' ','')		# formatowanie nazwy nowej tablicy 

	# dodawanie aotora do listy autorów
	cur.execute("INSERT INTO authors (AUTOR,AUTOR_NAME) VALUES(%s,%s)",(author,author_name,))

	# tworzenie nowej tabeli dla każdego autora
	cur.execute("""CREATE TABLE  {}   	
				(
				ID SERIAL PRIMARY KEY NOT NULL,
				SLOWO TEXT NOT NULL,
				LICZBA INT 
				)""".format(author))
	
	# wpisywanie danych do bazy danych
	for i in range(len(results)-1,-1,-1):		
		cur.execute("INSERT INTO " + author + " (SLOWO, LICZBA) VALUES (%s, %s)",(results[i][0],results[i][1],))	# uzupełnianie tabeli


	results.clear()				# czyszczenie zbioru posortowanych słow
	stats.clear()				# czyszczenie zbioru policzonych słów
	s.clear()					# czyszczenie zbioru słów



###################################################
# Liczenie wszystkich słów we wszystkich artykułach
for w in s_all:
		if w in stats:
			stats[w] += 1
		else:
			stats[w] = 1	

# sortowanie słów
sorted_stats = sorted(stats.items(), key=operator.itemgetter(1))
results = sorted_stats[-10::]

# Tworzenie tabeli z 10 najczestrzymi słowami
cur.execute("""CREATE TABLE stats	
				(
				ID SERIAL PRIMARY KEY NOT NULL,
				SLOWO TEXT NOT NULL,
				LICZBA INT 
				)""")

for i in range(len(results)-1,-1,-1):		
		cur.execute("INSERT INTO stats (SLOWO, LICZBA) VALUES (%s, %s)",(results[i][0],results[i][1],))	# uzupełnianie tabeli

conn.commit()