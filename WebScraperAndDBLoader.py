from bs4 import BeautifulSoup
import requests
import psycopg2

####################
#ŁĄCZENIE Z SERWEREM
DB_NAME = "test_db"
DB_USER = "postgres"
DB_PASS = "cytrus1"
DB_HOST = "localhost"
DB_PORT = "5432"

try:
	conn = psycopg2.connect(database = DB_NAME, user = DB_USER,
							password = DB_PASS, host = DB_HOST, port = DB_PORT)

	print("Polaczono")
	cur = conn.cursor()

except:
	print("Blad")


#####################################
#TWORZENIE NOWEJ TABLICY Z ARTYKUŁAMI
cur.execute("""
	CREATE TABLE Dane
	(
	ID SERIAL PRIMARY KEY NOT NULL,
	AUTOR TEXT NOT NULL,
	TEKST1 TEXT 
	)""")

print("Tablica utworzona")



#######################
#PRZEGLADANIE ARTYKUŁÓW
article_text = []									# tablica z trescią artykułu
blog_links = []										# tablica z linkami do stron z artykułami
source_link = 'https://teonite.com/pl/blog/'		# link pierwszej strony
blog_links.append(source_link)
text_columns = ['TEKST1']							# nazwa pierwszej kolumny z artykułami w bazie danych

for i in range(2, 5):
	blog_links.append(source_link + 'page/' + str(i) + '/index.html')		# tworzenie i dodawanie linków stron z artykułami

for home_link in blog_links:												# przeglądanie linków do stron z pojedynczymi artykółami
	source = requests.get(home_link).text 
	soup = BeautifulSoup(source, 'html.parser')
		
	for main_posts_page in soup.find_all('h2',class_="post-title"):			# odczytywanie klasy linków do artykółow z "post-title"
		for link in main_posts_page.find_all("a"):	
			link = link.get('href')
			
			while link.startswith('../'):									# "sklejanie" linków
				link = link[3::] 
			
			xlink = source_link + link[8::]
			print(xlink)																
			xsource = requests.get(xlink).text	
			target_page_soup = BeautifulSoup(xsource, 'html.parser')		# tworzenie linku docelowej strony z artykułem
	

			if target_page_soup.find(class_="author-name") != None:
				author = target_page_soup.find(class_="author-name").string.encode('iso8859-1').decode() 	# odczyt autora
			


				#DODAWANIE AUTORA DO BAZY DANYCH
				cur.execute("SELECT EXISTS(SELECT FROM Dane WHERE AUTOR = %s)",(author,)) 	# sprawdzenie czy autor znajduje sie w bazie danych
				row = cur.fetchone()[0]

				if row != True:																		# jeśli autor nie w bazie danych to dodaj					
					cur.execute("INSERT INTO Dane (AUTOR) VALUES (%s)",(author,))				

			

			#POBIERANIE TREŚCI ARTYKULU + TYTUŁ
			title = target_page_soup.find(class_="post-title").string.encode('iso8859-1').decode()
			article_text.append(title)			# dodwanie tytułu
			article_text.append(" ")			

			for text in target_page_soup.find_all('div', class_="post-content"):		# pobieranie fragmentu strony
				for x in text.strings:													# pobieranie treści artykułu
					x = x.split(" ")													# rozdzielanie tekstu na osobne słowa
					for l in x:															
						article_text.append(l.encode('iso8859-1','ignore').decode())	# odczyt pojedynczych słów				
						article_text.append(" ")


			#ZAPISYWANIE DANYCH W BAZIE DANYCH
			# text_columns - lista kolumn utworzonych w bazie danych
			# actTextColumn - aktualna kolumna do której ma być zapisana treść artykułu
			# new_column_name - nazwa nowoutworzonej kolumny

			for actTextColumn in text_columns:									
				cur.execute("SELECT " + actTextColumn + " FROM Dane WHERE AUTOR = '" + author + "'")	# sprawdzanie czy aktualna komórka jest pusta 
				row = cur.fetchone()[0]	
				
				if row == None:																								#jeśli pusta wpisz dane
					cur.execute("UPDATE Dane SET " + actTextColumn + " = %s WHERE AUTOR = %s",(''.join(article_text),author,))				
					break
				
				else:											#jeśli nie jest pusta utwórz nową kolumne
					if actTextColumn == text_columns[-1]:
						new_column_name = 'TEKST' + str(len(text_columns)+1)						
						text_columns.append(new_column_name)										# dodawanie kolumny do listy istniejacych kolumn
						cur.execute("ALTER TABLE Dane ADD COLUMN " + new_column_name +" TEXT")		# tworzenie nowej kolumny										
						print("Dodano kulumnę : " + new_column_name)										
					print("Pomijam")									

			article_text = []


conn.commit()
conn.close()
